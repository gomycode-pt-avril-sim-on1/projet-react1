// App.js
import React from 'react';
import { Navbar, Container, Nav, Card } from 'react-bootstrap';

const App = () => {
  return (
    <React.Fragment>
      <div className="App">
        <Navbar bg="dark" variant="dark">
          <Container>
            <Navbar.Brand href="#home">Mon App React</Navbar.Brand>
            <Nav className="me-auto">
              <Nav.Link href="#home">Accueil</Nav.Link>
              <Nav.Link href="#features">Fonctionnalités</Nav.Link>
              <Nav.Link href="#contact">Contact</Nav.Link>
            </Nav>
          </Container>
        </Navbar>

        <Container className="mt-3">
          <h1>Mon Titre</h1>

          <Card>
            <Card.Body>
              <Card.Title>Carte 1</Card.Title>
              <Card.Text>
                Contenu de la carte 1.
              </Card.Text>
            </Card.Body>
          </Card>

          <Card>
            <Card.Body>
              <Card.Title>Carte 2</Card.Title>
              <Card.Text>
                Contenu de la carte 2.
              </Card.Text>
            </Card.Body>
          </Card>

          <Card>
            <Card.Body>
              <Card.Title>Carte 3</Card.Title>
              <Card.Text>
                Contenu de la carte 3.
              </Card.Text>
            </Card.Body>
          </Card>
        </Container>
      </div>
    </React.Fragment>
  );
};

export default App;
